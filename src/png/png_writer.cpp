#include <ctime>
#include <malloc.h>
#include <png.h>
#include <stdexcept>
#include <string>
#include <stdio.h>

#include "png/png_writer.h"

namespace png
{

png_writer::png_writer(const std::string &path, int width, int height) :
	m_base_path(path),
	m_width(width),
	m_height(height)
{

}

png_writer::~png_writer()
{

}

void png_writer::write_png(const char* buffer)
{
	time_t time = std::time(0);
	struct tm *now = std::localtime(&time);
	char file_name[50];

	sprintf(file_name, "%d%02d%02d-%02d%02d%02d", now->tm_year + 1900, now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
	std::string full_path = m_base_path + std::string(file_name) + std::string(".png");

	FILE *fp;
	png_structp png_ptr;
	png_infop info_ptr;
	png_bytep row;

	fp = fopen(full_path.c_str(), "wb");
	if(fp == NULL)
	{
		printf("Failed to open file.");
		throw new std::runtime_error("Failed to open file");
	}

	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(png_ptr == NULL)
	{
		fclose(fp);
		throw new std::runtime_error("Failed to create png struct");
	}

	info_ptr = png_create_info_struct(png_ptr);
	if(info_ptr == NULL)
	{
		png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
		png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
		throw new std::runtime_error("Failed to create info struct");
	}

	png_init_io(png_ptr, fp);

	png_set_IHDR(png_ptr, info_ptr, m_width, m_height, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

	png_write_info(png_ptr, info_ptr);

	row = (png_bytep)malloc(3 * m_width * sizeof(char));

	for(int y = 0; y < m_height; y++)
	{
		for(int x = 0; x < m_width; x++)
		{
			int pos = x * 3;
			int buffer_pos = ((y * m_width) + (x)) * 4;
			row[pos] = buffer[buffer_pos];
			row[pos + 1] = buffer[buffer_pos + 1];
			row[pos + 2] = buffer[buffer_pos + 2];

			//printf("%d %d %d\n", row[x*3], row[(x * 3) + 1], row[(x * 3) + 1]);
		}
		png_write_row(png_ptr, row);
	}
	png_write_end(png_ptr, NULL);

	fclose(fp);
	png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
	png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
	free(row);
}

}
