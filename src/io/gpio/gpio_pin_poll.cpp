#include <fcntl.h>
#include <poll.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdexcept>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "io/gpio/gpio_pin_poll.h"
#include "io/gpio/pi_input_pin.h"

#define GPIO_FN_MAXLEN	32
#define POLL_TIMEOUT	500
#define RDBUF_LEN	5

namespace io
{

namespace gpio
{

gpio_pin_poll::gpio_pin_poll(const pi_input_pin *pin) :
	m_value(0),
	m_pin(pin),
	m_running(true)
{
	int ret = pthread_create( &m_thread, NULL, &gpio_pin_poll::poll_worker, (void*)this);
	if(ret)
	{
		fprintf(stderr,"Error - pthread_create() return code: %d\n",ret);
		throw std::runtime_error("Error - pthread_create()");
	}

	sem_init(&m_semaphore, 0, 0);
}

gpio_pin_poll::~gpio_pin_poll()
{
	m_running = false;
	printf("Waiting for thread to terminate\n");
	pthread_join(m_thread, NULL);
	printf("Thread terminated\n");
}

void gpio_pin_poll::poll()
{
	char fn[GPIO_FN_MAXLEN];
		int fd,ret;
		struct pollfd pfd;
		char rdbuf[RDBUF_LEN];

		memset(rdbuf, 0x00, RDBUF_LEN);
		memset(fn, 0x00, GPIO_FN_MAXLEN);

		snprintf(fn, GPIO_FN_MAXLEN-1, "/sys/class/gpio/gpio%s/value", m_pin->get_pin().c_str());
		fd=open(fn, O_RDONLY);
		if(fd<0) {
			perror(fn);
			throw std::runtime_error("Failed to open pin file");
		}
		pfd.fd=fd;
		pfd.events=POLLPRI;

		ret=read(fd, rdbuf, RDBUF_LEN-1);
		if(ret<0) {
			perror("read()");
			throw std::runtime_error("Failed to read pin file");
		}
		printf("value is: %s\n", rdbuf);
		while(m_running)
		{
			memset(rdbuf, 0x00, RDBUF_LEN);
			lseek(fd, 0, SEEK_SET);
			ret=::poll(&pfd, 1, POLL_TIMEOUT);
			if(ret<0) {
				perror("poll()");
				close(fd);
				throw std::runtime_error("Failed to poll pin file");
			}
			if(ret==0) {
				printf("timeout\n");
				continue;
			}
			ret=read(fd, rdbuf, RDBUF_LEN-1);
			if(ret<0) {
				throw std::runtime_error("Failed to read pin file");
			}
			int val = atoi(rdbuf);

			if(val == 0 || val == 1)
			{
				if(val != m_value)
				{
					m_value = val;
					sem_post(&m_semaphore);
				}
				else
				{
					printf("We interrupted but value was the same as before.\n");
				}
			}
			else
			{
				printf("We got a non 0/1 value...%d\n", val);
			}
		}

		close(fd);
}

void *gpio_pin_poll::poll_worker(void *context)
{
	while(((gpio_pin_poll *)context)->m_running)
	{
		printf("polling pin\n");
		try{
			((gpio_pin_poll *)context)->poll();
		}
		catch (std::runtime_error &e)
		{
		}
	}
	return 0;
}

}

}
