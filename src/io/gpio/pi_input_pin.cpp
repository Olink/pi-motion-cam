#include <stdio.h>
#include <fcntl.h>
#include <fstream>
#include <poll.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <stdexcept>
#include <unistd.h>

#include "io/gpio/pi_input_pin.h"

namespace io
{

namespace gpio
{

pi_input_pin::pi_input_pin(const std::string &pin) :
	m_pin(pin)
{
	set_gpio();
}

pi_input_pin::~pi_input_pin()
{
	unset_gpio();
}

void pi_input_pin::set_gpio()
{
	printf("Setting pin %s\n", m_pin.c_str());
	std::string export_str = "/sys/class/gpio/export";
	std::ofstream exportgpio(export_str.c_str());
	if (exportgpio < 0)
	{
		printf("Failed to open this pin.");
		throw std::runtime_error("Failed to open this pin");
	}
	else
	{
		exportgpio << m_pin; //write GPIO number to export
		exportgpio.close(); //close export file
	}

	std::string setdir_str ="/sys/class/gpio/gpio" + m_pin + "/direction";
	std::ofstream setdirgpio(setdir_str.c_str()); // open direction file for gpio
	if (setdirgpio < 0)
	{
		throw std::runtime_error("Failed to set pin direction.");
	}

	setdirgpio << "in"; //write direction to direction file
	setdirgpio.close(); // close direction file

	std::string setedge_str ="/sys/class/gpio/gpio" + m_pin + "/edge";
	std::ofstream setedgegpio(setedge_str.c_str()); // open direction file for gpio
	if (setedgegpio < 0)
	{
		throw std::runtime_error("Failed to set pin edge.");
	}

	setedgegpio << "both"; //write direction to direction file
	setedgegpio.close(); // close direction file
}

void pi_input_pin::unset_gpio()
{
	printf("Unsetting pin %s\n", m_pin.c_str());

	std::string export_str = "/sys/class/gpio/unexport";
	std::ofstream exportgpio(export_str.c_str());
	if (exportgpio < 0)
	{
		printf("Failed to close this pin.");
		throw std::runtime_error("Failed to close this pin");
	}
	else
	{
		exportgpio << m_pin; //write GPIO number to export
		exportgpio.close(); //close export file
	}
}

std::string pi_input_pin::get_value()
{
	std::string getval_str = "/sys/class/gpio/gpio" + m_pin + "/value";
	std::ifstream getvalgpio(getval_str.c_str());// open value file for gpio
	if (getvalgpio < 0){
		throw std::runtime_error("Failed to read the pin");
	}

	std::istreambuf_iterator<char> eos;
	std::string result(std::istreambuf_iterator<char>(getvalgpio), eos);
	return result;
}

const std::string pi_input_pin::get_pin() const
{
	return m_pin;
}

}

}
