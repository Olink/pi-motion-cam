#include <cstdio>
#include <semaphore.h>
#include <stdexcept>
#include <string>
#include <unistd.h>

#include "camera/camera.h"
#include "io/gpio/gpio_pin_poll.h"
#include "io/gpio/pi_input_pin.h"
#include "png/png_writer.h"

void dump_data_to_png(const void* buffer)
{
	const char *buf = static_cast<const char *>(buffer);

	const std::string path("./png/");
	png::png_writer png(path, 1280, 720);
	png.write_png(buf);
	delete buf;
}

int main()
{
	const std::string pin("17");
	try
	{
		io::gpio::pi_input_pin pin17(pin);
		io::gpio::gpio_pin_poll poll(&pin17);

		int i = 0;
		while(true)
		{
			sem_wait(&poll.m_semaphore);
			printf("Pin posted %d\n", poll.m_value);

			if(poll.m_value != 1)
				continue;

			CCamera *camera = StartCamera(1280, 720, 30, 1, true);

			const void* frame_data; int frame_sz;

			if(camera->BeginReadFrame(0, frame_data, frame_sz))
			{
				if(frame_sz > 1)
				{
					dump_data_to_png(frame_data);
					i++;
				}
			}
			StopCamera();
		}
	}
	catch (std::runtime_error &e)
	{
		StopCamera();
		return 1;
	}
	return 0;
}
