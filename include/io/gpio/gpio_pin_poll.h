#ifndef GPIO_PIN_POLL_H_INCLUDED
#define GPIO_PIN_POLL_H_INCLUDED

#include <pthread.h>
#include <semaphore.h>

#include "io/gpio/pi_input_pin.h"
namespace io
{

namespace gpio
{

class gpio_pin_poll
{
public:
	gpio_pin_poll(const pi_input_pin *pin);
	virtual ~gpio_pin_poll();

	sem_t m_semaphore;
	int m_value;
private:
	void poll();

	static void *poll_worker(void *context);

	const pi_input_pin *m_pin;
	bool m_running;
	pthread_t m_thread;
};

}

}

#endif
