#ifndef PI_GPIO_H_INCLUDED
#define PI_GPIO_H_INCLUDED

#include <string>

namespace io
{

namespace gpio
{

class pi_input_pin
{
public:
	pi_input_pin(const std::string &pin);
	virtual ~pi_input_pin();
	std::string get_value();
	const std::string get_pin() const;
private:
	virtual void set_gpio();
	virtual void unset_gpio();

	std::string m_pin;
};

}

}

#endif
