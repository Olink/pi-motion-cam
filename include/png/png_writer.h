#ifndef PNG_WRITER_H_INCLUDED
#define PNG_WRITER_H_INCLUDED

#include <string>

namespace png
{

class png_writer
{
public:
	png_writer(const std::string &path, int width, int height);
	virtual ~png_writer();
	void write_png(const char *buffer);
private:
	std::string m_base_path;
	int m_width;
	int m_height;
};

}

#endif
